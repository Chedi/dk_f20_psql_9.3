dk_f20_psql_9.3
===============================

Docker container for postgresql 9.3 + postgis 2.1.1 on fedora 20


##Info:
This Dockerfile creates a container running PostGIS 2.1.1 in PostgreSQL 9.3 based on the fedora 20 distro

* expose port 5432
* initializes a database in /var/lib/pgsql/data
* superuser in the database: postgres (no authentifcation needed)


##Usage:

You can rebuild the container by yourself using :

```
docker build git://github.com/Chedi/dk_f20_psql_9.3.git
```

or directly download it from the docker index 

```
docker pull chedi/f20-psql-9.3
```

once the image ready you can run it with
```
CONTAINER=$(sudo docker run -d -t chedi/f20-psql-9.3) 
```

You can get the container ip using this command:


```
CONTAINER_IP=$(sudo docker inspect $CONTAINER | grep IPAddress | awk '{ print $2 }' | tr -d ',"')
```

And finally test the good working of the database by using:
```
psql -h $CONTAINER_IP -U postgres
```

##Persistence:
Begin by creating a new folder in the host machine

```
mkdir -p /var/data/f20-psql-9.3
```

Then when running the container add the sepecification of the host volume using the -v option

```
CONTAINER=$(sudo docker run -t -d -v /var/data/f20-psql-9.3:/var/lib/pgsql:rw chedi/f20-psql-9.3)
```

##Port mapping
If for any reason you don't want to bother with the dynamic container ip, you can map the postgresql port to one 
on the host machine. Just notice, that for security reason I recommend to bind the port to the 127.0.0.1 ip so it
can not be visible outside the machine.


```
CONTAINER=$(sudo docker run -t -d -p 127.0.0.1:9999:5432 -v /var/data/f20-psql-9.3:/var/lib/pgsql:rw chedi/f20-psql-9.3)
```

>In this example we map the postgresql 5432 port on the container to the host machine 9999 port. We've chosen this particular port not to colide with an existing postgresql instance.

We will then connect to the server on the mapped port

```
psql -h 127.0.0.1 -U postgres -p 9999
```


##Meta:
Built with docker-io 0.7.6 on Centos 6.4
